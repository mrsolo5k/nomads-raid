﻿using UnityEngine;
using System.Collections;

public class Tower : ShootableObject {

	void Start () {
		base.Start ();
	}

	void Update () {
		base.Update ();

		if (intervalElapsed <= .0f) {

			GameObject nearest = FindNearest ();

			if (nearest && InRange(nearest.transform)) {
				Shoot (nearest);
			}
			intervalElapsed = interval;
		}
		intervalElapsed -= Time.deltaTime;
	}
}
