﻿using UnityEngine;
using System.Collections;

public abstract class PlacableObject : MonoBehaviour {

	public int price = 50;
	public float buildCooldown = 5.0f;

	private bool demolishInitiated = false;
	private float t = 0;

	public virtual void PerformPostPlaceAction () {
	}

	public void Demolish() {
		Destroy (gameObject, 3.0f);
		demolishInitiated = true;
		GetComponent<Collider> ().enabled = false;
	}

	protected void Start () {

	}

	public void ReduceColor() {
		Component[] sprites = GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer sprite in sprites) {
			sprite.color -= new Color(0.0f,0.0f,0.0f,0.015f * GameManager.fixedTimeFactor()); // TODO: hardcoded
		}
	}
		
	protected void FixedUpdate () {
		if (demolishInitiated) {
			ReduceColor ();
		}
	}

	protected void Update () {
	}
}
