﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	
	// Configuration

	public int coins = 50;
	public int waveNumber = 10;
	public int levelFactor = 0;
	public float timeToNextWave = 10.0f;
	public int missedMax = 1;
	public float gameDuration;

	private float elapsedTime;

	public string[] waveTitles = { "Next", "Next", "Wave", "In", "3", "2", "1", "GO!" };

	public GameObject[] enemyPrefabs;
	public GameObject particlePrefab;
	public Transform spawnPoint; // TODO: make multiple
	public Transform target; // TODO: make multiple / find closest

	private List<GameObject> missedList = new List<GameObject>();

	public List<GameObject> enemies = new List<GameObject>();
	private static GameManager self;

	// GUI & GUI related

	private float elapsedToNextWave;

	public float timeScale;

	public Text coinsText;
	public Text enemiesText;
	public Text waveText;
	public GameObject missedPrefab;

	public Transform missedOffset;
	public Transform canvas;

	private ButtonDelegate callbackAfterBuild;
	private Cursor activeCursor = null;

	private object _sync = new object();

	// For statistics

	public int overallKills = 0;

	// camera shaking params

	private Vector3 originPosition;

	public float shakeDecay;
	public float shakeIntensity;

	private PlacableObject lastPlacedObject;

	void Awake() {
		self = this;
	}

	public void Shake(){
		originPosition = transform.position;
		shakeIntensity = .9f;
		shakeDecay = .016f;
	}

	void Start () {

		Time.timeScale = timeScale;

		for (int i = 0; i < missedMax; i++) {
			GameObject go = (GameObject)Instantiate (missedPrefab, missedOffset.position, Quaternion.identity);
			go.transform.parent = canvas;
			go.GetComponent<RectTransform>().localPosition += new Vector3(30.0f,0.0f,0.0f) * i;
			go.transform.localScale = Vector3.one;
			missedList.Add (go);
		}

		elapsedToNextWave = timeToNextWave;
		elapsedTime = gameDuration;
	}

	public void InitateNextWave() {

		// TODO: so random ;)
		int numberOfVillains = waveNumber * (waveNumber + levelFactor);

		for (int i = 0; i < numberOfVillains; i++) {
			Vector3 position;
			position.x = spawnPoint.position.x + Random.Range (-spawnPoint.localScale.x, spawnPoint.localScale.x);
			position.y = spawnPoint.position.y + Random.Range (-spawnPoint.localScale.y, spawnPoint.localScale.y);
			position.z = 0;

			int typeOfEnemy = 0;

			int randomType = Random.Range (0, waveNumber); // TODO: higher wave, more chances for cavalry
			typeOfEnemy = randomType % enemyPrefabs.Length;

			GameObject instance = (GameObject)Instantiate (enemyPrefabs[typeOfEnemy], position, Quaternion.identity);
			instance.GetComponent<AIController> ().SetTarget (target);

			// A little bit of balance
			instance.GetComponent<AIController> ().maxHealth += (waveNumber-1) * 10;

			enemies.Add (instance);
		}

		if (waveText)
			waveText.text = waveNumber + "";

		waveNumber++;
	}

	public static GameManager Get() {
		return self;
	}
		
	public List<GameObject> GetEnemies() {
		return enemies;
	}

	public void DemolishLastPlacedObject() {
		if (lastPlacedObject) {
			lastPlacedObject.GetComponent<PlacableObject> ().Demolish ();

			UpdateGraphAroundObject (lastPlacedObject.gameObject);
			spawnPoint.GetComponent<LastPathProtector> ().RecalculatePath ();
			activeCursor.Reset (); // TODO: test it!
		}
	}

	public void KillEnemy(GameObject enemy) {
		lock (_sync) {
			enemies.Remove (enemy);
			Destroy (enemy, 2.0f);
			overallKills++;
		}
	}

	void Update () {
		if(coinsText)
			coinsText.text = coins + " £";

		if (enemiesText)
			enemiesText.text = enemies.Count + "";

		if (elapsedToNextWave >= 0 && enemies.Count == 0) {
			int index = (int)(((timeToNextWave-elapsedToNextWave)/timeToNextWave)*waveTitles.Length);
			if(index < waveTitles.Length)
				waveText.text = waveTitles[index];
			elapsedToNextWave -= Time.deltaTime;
		}

		if (enemies.Count == 0 && elapsedToNextWave <= 0) {
			elapsedToNextWave = timeToNextWave;
			InitateNextWave();
		}

		if(shakeIntensity > 0){
			transform.position = originPosition + Random.insideUnitSphere * shakeIntensity;
			shakeIntensity -= shakeDecay;
		}

		elapsedTime -= Time.deltaTime;
		if (elapsedTime < 0 && enemies.Count == 0) {
			OnGameEnd (1);
		}
	}

	private void UpdateGraphAroundObject(GameObject go) {
		GraphUpdateObject guo = new GraphUpdateObject(go.GetComponent<Collider>().bounds);
		guo.updatePhysics = true;
		AstarPath.active.UpdateGraphs (guo);
	}

	private void UpdateAI(GameObject go) {

		if (!go.GetComponent<Collider> ())
			return;

		UpdateGraphAroundObject (go);

		// Sorting by distance to newly created Tower
		enemies.Sort (delegate(GameObject x, GameObject y) {

			float distanceX = (go.transform.position-x.transform.position).magnitude;
			float distanceY = (go.transform.position-y.transform.position).magnitude;
			return distanceX.CompareTo(distanceY);
		});

		foreach(GameObject enemy in enemies) {

			if (enemy) {
				enemy.GetComponent<AIController>().ScheduleRecalculatePath (go.transform);
			}
		}

		spawnPoint.GetComponent<LastPathProtector> ().RecalculatePath ();
	}

	public void ChangeAccountBalance(int amount) {
		lock (_sync) {
			this.coins += amount;
		}
	}

	public void IncrementMissed() {
		lock (_sync) {
			if (missedList.Count > 0) {
				GameObject go = missedList [missedList.Count - 1];
				missedList.RemoveRange (missedList.Count - 1, 1);
				Object.Destroy (go);
			}
			if (missedList.Count <= 0) {
				OnGameEnd (0);
			}
		}
	}

	public int GetMissed () {
		return missedList.Count;
	}

	public void SetActiveCursor (Cursor cursor, ButtonDelegate buildCallback) {
		if(activeCursor)
			activeCursor.gameObject.SetActive (false);
		if (cursor) {
			cursor.Reset ();
		}

		activeCursor = cursor;
		callbackAfterBuild = buildCallback;
	}

	public Cursor GetActiveCursor() {
		return activeCursor;
	}

	public List<GameObject> FindEnemiesInRange(Transform center, float range) {

		List<GameObject> enemiesInRange = new List<GameObject> ();

		foreach(GameObject enemy in enemies) {
			if ((enemy.transform.position - center.position).magnitude < range) {
				enemiesInRange.Add (enemy);
			}
		}

		return enemiesInRange;
	}

	private int GetCost(GameObject objectPrefab) {
		return objectPrefab.GetComponent<PlacableObject> ().price;
	}

	public bool CanAfford(GameObject objectPrefab) {
		return coins - GetCost(objectPrefab) >= 0;
	}
		
	public void BuildTower(Vector2 snapPoint) {

		if (activeCursor) {
			GameObject objectPrefab = activeCursor.GetPrefab();

			if (objectPrefab) {
				float buildCooldown = objectPrefab.GetComponent<PlacableObject> ().buildCooldown;

				if (CanAfford(objectPrefab)) {

					if (callbackAfterBuild (buildCooldown)) {

						GameObject go = (GameObject)Instantiate (objectPrefab, new Vector3 (snapPoint.x, snapPoint.y, -1.0f), Quaternion.identity);
						go.GetComponent<PlacableObject> ().PerformPostPlaceAction ();

						lastPlacedObject = go.GetComponent<PlacableObject> ();
						ChangeAccountBalance (-GetCost(objectPrefab));
						UpdateAI (go);
					}
				}
			}
		}
	}

	public void TriggerEvent(Vector2 snapPoint) {
		BuildTower (snapPoint);
	}

	public void OnGameEnd(int win) {
		PlayerPrefs.SetInt ("coins", coins);
		PlayerPrefs.SetInt ("kills", overallKills);
		PlayerPrefs.SetInt ("missed", missedMax - GetMissed());
		PlayerPrefs.SetInt ("win", win);
		PlayerPrefs.SetInt ("level", levelFactor+1);

		PlayerPrefs.Save ();

		SceneManager.LoadScene ("Results");
	}

	public static float fixedTimeFactor() {
		return Time.fixedDeltaTime * 50;
	}
}
