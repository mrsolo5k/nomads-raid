﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class LastPathProtector : MonoBehaviour {

	public Transform target;

	private Seeker seeker;

	void Start () {
		seeker = this.GetComponent<Seeker> ();
	}

	void Update () {

	}

	public void RecalculatePath() {
		seeker.StartPath (transform.position,target.position, OnPathComplete);
	}

	public void OnPathComplete (Path p) {
		if (!p.error) {
			// can't find path anymore, block player to build in this location
		} else {
			GameManager.Get ().DemolishLastPlacedObject ();
		}
	}
}
