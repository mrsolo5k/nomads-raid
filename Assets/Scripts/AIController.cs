﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class AIController : MonoBehaviour {

	public float speed = 1;
	public float maxHealth = 15;
	public int price = 15;
	public bool mock = false;
	public Vector3 velocity;

	public Animator auxAnimator;
	public Transform target;
	public float nextWaypointDistance = 3;

	private Animator animator;
	private float actualSpeed;
	private Seeker seeker;
	private Path path;
	private float health;

	public float proximityThreshold = 128.0f;

	private int currentWaypoint;

	void Awake () {
		animator = this.GetComponent<Animator> ();
		seeker = this.GetComponent<Seeker> ();

		health = maxHealth;
	}

	// TODO: timeout for tower to build
	public void ScheduleRecalculatePath(Transform newObject) {
		//recalculationRequested=1;
		if (Vector3.Distance (transform.position, newObject.position) < proximityThreshold) {
			path = null;
			seeker.StartPath (transform.position,target.position, OnPathComplete);
		} else {
			//path = null;
			seeker.StartPath (transform.position,target.position, OnPathCompleteAndNextWaypoint);
		}
	}

	public void OnPathComplete (Path p) {
		if (!p.error) {
			path = p;
		} else {
			ReduceHealth (maxHealth);
		}
		currentWaypoint = 0;
	}

	public void OnPathCompleteAndNextWaypoint (Path p) {
		if (!p.error) {
			path = p;
			// Find nearest waypoint after recalculations while still moving
			currentWaypoint = 0;
			float closestDistance = Vector3.Distance (transform.position, path.vectorPath [currentWaypoint]);

			while (currentWaypoint < path.vectorPath.Count - 1 &&
			      Vector3.Distance (transform.position, path.vectorPath [currentWaypoint + 1]) < closestDistance) {
				closestDistance = Vector3.Distance (transform.position, path.vectorPath [currentWaypoint++]);
			}

			if (currentWaypoint < path.vectorPath.Count - 1)
				currentWaypoint++;
		}
	}

	public void ReduceHealth(float hitPoints) {
		health -= hitPoints;
		if (health <= 0) {
			OnDeath ();
		}
	}

	public void OnDeath() {
		if (GameManager.Get ().particlePrefab) {
			GameObject go = (GameObject) Instantiate (GameManager.Get ().particlePrefab, transform.position, Quaternion.identity);
		}
		GameManager.Get ().KillEnemy (gameObject);
		GameManager.Get ().ChangeAccountBalance (price);
	}

	void FixedUpdate () {

		if (health <= 0) {
			transform.localScale = Vector3.Lerp (transform.localScale, Vector3.zero, Time.fixedDeltaTime*5.0f);
			Move (Vector3.zero);
			return;
		}

		if (path == null) {
			if(!mock) Move(new Vector3(.0f,.0f,.0f)); // TODO: when using in presentation screen, I don't want to prevent movement
		 	return;
		}
			
		if (currentWaypoint >= path.vectorPath.Count) {
			GameManager.Get().IncrementMissed();
			// TODO: small redundancy
			GameManager.Get ().KillEnemy (gameObject);
			Move (Vector3.zero);
			Destroy (gameObject);
			return;
		}
		velocity = (path.vectorPath [currentWaypoint] - transform.position).normalized;
		velocity *= speed;
		Move (velocity);

		if (Vector3.Distance (transform.position, path.vectorPath [currentWaypoint]) < nextWaypointDistance) {
			currentWaypoint++;
		}

		float height = GetComponent<SpriteRenderer> ().bounds.size.y;

		GetComponent<SpriteRenderer> ().sortingOrder = (int)(-transform.position.y + (height/2)) + 2000;
	}

	public void SetTarget(Transform target) {
		this.target = target;

		currentWaypoint = 0;
		seeker.StartPath (transform.position,target.position, OnPathComplete);
	}

	public void Move(Vector3 moveVector) {

		// TODO: consider rendering to texture (shadows)

		if ( Mathf.Abs(moveVector.y) > Mathf.Abs(moveVector.x) && moveVector.y > 0) {
			animator.SetInteger ("Direction", 2);
			animator.SetFloat ("Speed", speed);
			if (auxAnimator) {
				auxAnimator.SetInteger ("Direction", 2);
				auxAnimator.SetFloat ("Speed", speed);
			}
		} else if ( Mathf.Abs(moveVector.y) > Mathf.Abs(moveVector.x) && moveVector.y < 0) {
			animator.SetInteger ("Direction", 0);
			animator.SetFloat ("Speed", speed);
			if (auxAnimator) {
				auxAnimator.SetInteger ("Direction", 0);
				auxAnimator.SetFloat ("Speed", speed);
			}
		} else if ( Mathf.Abs(moveVector.x) >= Mathf.Abs(moveVector.y) && moveVector.x > 0) {
			animator.SetInteger ("Direction", 3);
			animator.SetFloat ("Speed", speed);
			if (auxAnimator) {
				auxAnimator.SetInteger ("Direction", 3);
				auxAnimator.SetFloat ("Speed", speed);
			}
		} else if ( Mathf.Abs(moveVector.x) >= Mathf.Abs(moveVector.y) && moveVector.x < 0) {
			animator.SetInteger ("Direction", 1);
			animator.SetFloat ("Speed", speed);
			if (auxAnimator) {
				auxAnimator.SetInteger ("Direction", 1);
				auxAnimator.SetFloat ("Speed", speed);
			}
		} else {
			animator.SetFloat("Speed", 0.0f);
			if (auxAnimator) {
				auxAnimator.SetFloat ("Speed", 0.0f);
			}
		}
		transform.position += moveVector * GameManager.fixedTimeFactor();
	}
}
