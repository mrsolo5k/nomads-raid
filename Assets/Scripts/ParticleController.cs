﻿using UnityEngine;
using System.Collections;

public class ParticleController : MonoBehaviour {

	private float timeout = 1.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeout -= Time.deltaTime;
		if (timeout < 0) {
			//GetComponent<ParticleSystem> ().duration = false;
			Object.Destroy (gameObject, 2.0f);
		}
	}
}
