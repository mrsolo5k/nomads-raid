﻿using UnityEngine;
using System.Collections;

public class CameraAspect : MonoBehaviour {

	public float referenceRatio = 16.0f/9.0f;
	public float screenRatio;

	// Use this for initialization
	void Start () 
	{
		Camera camera = GetComponent<Camera>();

		screenRatio = (float) Screen.width / Screen.height;

		float oldSize = camera.orthographicSize;

		camera.orthographicSize =  oldSize * (float)(referenceRatio / screenRatio);
	}
}
