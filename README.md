# README #

Nomads' Raid development README

### Overview ###

Game is a 2D Top Down tower defense style game placed in medieval fantasy world.
The goal is to protect the castle from the waves of nomads. Their obly goal is to reach
the castle. Game is over when 7 or more Nomads get to the castle. You win the game
when the time is up (4 mins) and nomads 7 haven't reachead to the castle yet.

You can build almost everywhere but not when you collide with other buildings/
static obstacles or enemies with only one exception for meteor impact, you can place
it on enemies.

There is only one more rule (I hope). You CAN'T BLOCK the LAST possible PATH for enemies
to reach the target. When you do it, tour newly built structure SHALL BE DESTROYED!
You can block the individual or group of units though, they are simply being killed then.

### Folders structure ###

* Src/NomadsRaid - Unity Project

Builds:

* Android
* Mac
* Windows

### Approach ###

I decided to stick to 2D Top Down style because it was easy to find the appropriate
assets and the integrate them altogether inside them keeping them consistent.

Usage of powerful map editor Tiled and unity plugin for it - UTiled, makes creation of
levels a fun and it's really quick.

I wanted the area of gameplay to be more open, instead of fixed paths, that's I decided
to utilise pathfinding algorithm A* for enemy units to search for the shortest path to
the target.

It was definately not obvious for me how to solve the issue with the last path, but
I ended up implementing the simple sollution with one global path from spawn point to
target when testing for the last path.

Thus is a 2D game I had to focus on layering and proper ordering which sometime got a
little complicated, but I tried to keep as much consistency as possible. There are
still some parts of the code that are workarounds and need to be rewrite to be more
readable/reusable though.

I was trying to refactor after each minor steps to keep the code more readable and
ready for additional features I might have wanted to add. W tried to follow some
of those goals when I was planning the structure and abstractions:

* reusable and adjustable assests for different
* keep it simple
* as low redundancy as possible

### Area of optimization###

I'll just bulletpoint:

* paths could be computed not for each unit in the same time (I could specify some delay 
fo units that are further from the newly bulit structure than others)
* sharing paths
* units drawing (shadows, consider max number of units)
* ...

### Next Steps ###

* Better balance to make game more enjoyable
* More levels
* Yet more refactoring to make specific assetss more re-usable
* More units
* Right now the max health of the units is incresing with the next waves. I am not sure 
if it's a way to go and also it should be visualized somehow
* More power-ups/Towers
* Upgrading/Demolishing Towers
* Add some sounds
* Some sprites issues in terms of ovelapping/collisions/placing
* Layering and ordering could be imroved in some circumstances
