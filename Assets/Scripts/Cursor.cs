﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Cursor : MonoBehaviour {

	public GameObject prefab;
	private int colliding = 0;
	public bool canPlaceOnEnemies = false;

	private bool coolingDown = false;

	void Start () {
		SetColor (new Color (0.0f, 1.0f, 0.0f, 0.5f));
	}

	void Update () {
		// TODO: hardcoded colors
		if (colliding > 0) {
			SetColor (new Color (1.0f, 0.0f, 0.0f, 0.5f));
		} else {
			if (GameManager.Get ().CanAfford (prefab) && !coolingDown) {
				SetColor (new Color (0.0f, 1.0f, 0.0f, 0.5f));
			} else {
				SetColor (new Color (0.3f, 0.3f, 0.3f, 0.5f));
			}
		}
	}

	public GameObject GetPrefab() {
		return prefab;
	}

	public bool IsColliding() {
		return colliding > 0;
	}

	public void SetCoolindDown (bool coolingDown) {
		this.coolingDown = coolingDown;
	}

	public void Reset() {
		colliding = 0;
	}

	public void SetColor(Color color) {
		Component[] sprites = GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer sprite in sprites) {
			sprite.color = color;
		}
	}

	void OnTriggerEnter(Collider collider) {
		// FIXME: try to do it more efficient
		// GROUND == 8 ENEMIES == 10 CURSOR == 11
		if (collider.gameObject.layer == 8 
		 || collider.gameObject.layer == 11
		 || (collider.gameObject.layer == 10 && canPlaceOnEnemies))
			return;

		colliding ++;
	}

	void OnTriggerExit(Collider other) {
		// FIXME: try to do it more efficient
		// GROUND == 8 ENEMIES == 10 CURSOR == 11
		if (other.gameObject.layer == 8 
			|| other.gameObject.layer == 11
			|| (other.gameObject.layer == 10 && canPlaceOnEnemies))
			return;

		colliding --;
	}
}
