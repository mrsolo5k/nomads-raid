﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Meteor : Arrow {

	private bool hit = false;
	public GameObject impactSprite;

	void Start () {
	
	}

	void FixedUpdate () {
		if (target && !hit) {
			if ((transform.position - target.transform.position).magnitude > threshold) {
				transform.position += (target.transform.position - transform.position).normalized * speed;
			} else {
				List<GameObject> enemies = GameManager.Get ().FindEnemiesInRange (target.transform, distance);
				foreach(GameObject enemy in enemies) {
					enemy.GetComponent<AIController>().ReduceHealth (damage);
				}
				target.GetComponent<SpriteRenderer> ().enabled = true;
				GetComponent<SpriteRenderer> ().enabled = false;
				impactSprite.GetComponent<SpriteRenderer> ().enabled = true;
				hit = true;
				Object.Destroy (gameObject, 1.0f);
			}
		}
	}

	void Update () {
		if(hit) {
			target.GetComponent<SpriteRenderer> ().color = 
				Color.Lerp (target.GetComponent<SpriteRenderer> ().color, new Color (1.0f, 1.0f, 1.0f), Time.deltaTime);
			GameManager.Get().Shake ();
		}
	}
}
