﻿using UnityEngine;
using System.Collections;

public class MeteorImpact : ShootableObject {

	private bool justOnce = true;
	public GameObject craterSprite;

	// Use this for initialization
//	void Start () {
//		
//	}

	// Update is called once per frame
	void Update () {
		if (!justOnce)
			return;

		if (intervalElapsed <= .0f) {

			Shoot (craterSprite);
			justOnce = false;
		}
		intervalElapsed -= Time.deltaTime;
	}
}
