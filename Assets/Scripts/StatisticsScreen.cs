﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatisticsScreen : MonoBehaviour {

	public Text coinsText;
	public Text killsText;
	public Text missedText;

	public GameObject winScreen;
	public GameObject gameOverScreen;

	private bool win = false;
	public int numberOfLevels = 2;

	// Use this for initialization
	void Start () {
		win = (PlayerPrefs.GetInt ("win") == 1);
		winScreen.SetActive (win);
		gameOverScreen.SetActive (!win);

		coinsText.text = PlayerPrefs.GetInt ("coins") + " £";
		killsText.text = PlayerPrefs.GetInt ("kills").ToString();
		missedText.text = PlayerPrefs.GetInt ("missed").ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			if (win) {
				SceneManager.LoadScene ("GameScreen" + ((numberOfLevels+1)-PlayerPrefs.GetInt ("level"))); // TODO: hardcoded

			} else {
				SceneManager.LoadScene ("GameScreen" + PlayerPrefs.GetInt ("level"));
			}
		}
	}
}
