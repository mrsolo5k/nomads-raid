﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class PresentationScreenController : MonoBehaviour {

	public GameObject[] enemyPrefabs;
	private List<GameObject> enemies = new List<GameObject>();

	public int numberOfVillains;
	public Transform spawnPoint;

	public Text firstText;
	public Text secondText;

	public float timeScale = 0.3f;
	public float timeOfCameraMovement = 10.0f;
	public float timeForFirstTextToAppear = 2.0f;
	public float timeForSecondTextToAppear = 3.0f;
	public float timeFromStart = 0.0f;
	public float blinkTime = 5.0f;

	void Start () {

		Time.timeScale = timeScale;

		for (int i = 0; i < numberOfVillains; i++) {
			Vector3 position;
			position.x = spawnPoint.position.x + 64 * (int)i/4 + Random.insideUnitCircle.x * spawnPoint.localScale.x;
			position.y = spawnPoint.position.y + 64 * (int)(i%4 -2) + Random.insideUnitCircle.y * spawnPoint.localScale.y;
			position.z = 0;

			int typeOfEnemy = Random.Range (1, 2);

			GameObject instance = (GameObject)Instantiate (enemyPrefabs[typeOfEnemy], position, Quaternion.identity);
			AIController aiController = instance.GetComponent<AIController> ();
			aiController.mock = true;
			enemies.Add (instance);
		}
	}

	void FixedUpdate () {
		foreach(GameObject enemy in enemies) {
			AIController aiController = enemy.GetComponent<AIController> ();
			aiController.Move (new Vector3(aiController.speed,0.0f,0.0f) * Time.fixedDeltaTime * 50 );
		}
	}

	void Update () {

		if (Input.GetButtonDown ("Fire1")) {
			SceneManager.LoadScene ("GameScreen1");
			Time.timeScale = 1.0f;
		}

		if (timeFromStart < timeOfCameraMovement) {
			transform.position = Vector3.Lerp (transform.position, transform.position + new Vector3 (10.0f, 0.0f, 0.0f), Time.deltaTime);
			timeOfCameraMovement -= Time.deltaTime;
		}
		if (timeFromStart > timeForFirstTextToAppear)
			firstText.gameObject.SetActive (true);
		if (timeFromStart > timeForSecondTextToAppear)
			secondText.gameObject.SetActive (true);
		
		timeFromStart += Time.deltaTime;
	}
}
