﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerInput : MonoBehaviour {

	// Mainly because of port for Android
	public GameObject touchCollider;

	void Start () {
	
	}
	void Update() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if (EventSystem.current.IsPointerOverGameObject ()) {
			return;
		}

		Cursor cursor = GameManager.Get ().GetActiveCursor ();

		if(cursor)
			touchCollider.transform.position = cursor.transform.position;

		if (Physics.Raycast(ray, out hit)) {

			float curSize = 64;

			if (hit.collider.GetComponent<Cursor> () || hit.collider == touchCollider.GetComponent<Collider>()) {
				if (Input.GetButtonDown ("Fire1")) {
					if (cursor && !cursor.IsColliding()) {
						GameManager.Get ().TriggerEvent (cursor.transform.position);
					}
				}
			}

			Vector2 snapPoint;
			snapPoint.x = (int)(hit.point.x / 32) * 32 - 16;
			snapPoint.y = (int)(hit.point.y / 32) * 32 - 16;

			if (cursor) {
				cursor.transform.position = new Vector2(snapPoint.x+curSize/2,snapPoint.y-curSize/2);
				cursor.gameObject.SetActive (true);
			}
		}
	}
}
