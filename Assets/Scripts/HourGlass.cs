﻿using UnityEngine;
using System.Collections;

public class HourGlass : MonoBehaviour {

	public GameObject sandUp;
	public GameObject sandDown;
	public GameObject sandParticle;

	public float startY = 4;
	public float stopY = -39;
	public float particleSpeed = 80;
	public float distanceToMove = 20.0f;

	private float speed;

	void Start () {
		sandParticle.transform.position = new Vector3 (0.0f, startY, 1.0f);
		speed = (float) distanceToMove / GameManager.Get ().gameDuration;
	}

	void Update () {
		sandUp.transform.localPosition += new Vector3 (0.0f, -Time.deltaTime*speed, 0.0f);
		sandDown.transform.localPosition += new Vector3 (0.0f, -Time.deltaTime*speed, 0.0f);

		if (sandParticle.transform.localPosition.y > stopY) {
			sandParticle.transform.localPosition -= new Vector3 (0.0f, Time.deltaTime*particleSpeed, 0.0f);
		} else {
			sandParticle.transform.localPosition = new Vector3 (0.0f, startY, 1.0f);
		}
	}
}
