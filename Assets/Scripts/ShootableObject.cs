﻿using UnityEngine;
using System.Collections;

public class ShootableObject : PlacableObject {

	public GameObject arrowPrefab;
	public Transform shootingStarpoint;

	//public float accuracy; // TODO: do good shooting
	public float distance;
	public float interval = 3.0f;
	protected float intervalElapsed;

	protected void Start () {
		base.Start ();
		intervalElapsed = interval;
	}

	protected GameObject FindNearest() {

		float smallestDistance = float.MaxValue;
		GameObject nearest = null;

		foreach (GameObject enemy in GameManager.Get ().GetEnemies()) {
			if (enemy) {
				float distance = Vector3.Distance (enemy.transform.position, transform.position);
				if (distance < smallestDistance) {
					nearest = enemy;
					smallestDistance = distance;
				}
			}			
		}

		return nearest;
	}

	protected void Shoot(GameObject target) {
		GameObject newArrow = (GameObject)Instantiate (arrowPrefab, shootingStarpoint.position, Quaternion.identity);
		newArrow.GetComponent<Arrow> ().target = target;		
	}

	protected bool InRange(Transform target) {
		return (target.position - shootingStarpoint.position).magnitude < distance;
	}

	protected void Update () {
		base.Update ();
	}
}
