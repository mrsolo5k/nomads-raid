﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public delegate bool ButtonDelegate(float time);

public class CooldownButton : MonoBehaviour {

	public int typeOfBuidling = 0;
	ButtonDelegate buttonDelegate;

	public Cursor cursor;

	private float cooldownTime = .0f;
	private float currentTime = .0f;

	bool PerformFunctionality(float time) {
		if(currentTime <= 0) {
			GetComponent<Image> ().fillAmount = 0;
			cooldownTime = time;
			currentTime = time;
			GetComponent<Button>().interactable = false;
			cursor.SetCoolindDown (true);
			return true;
		}
		return false;
	}

	// Use this for initialization
	void Start () {
		buttonDelegate = PerformFunctionality;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentTime > 0) {
			currentTime -= Time.deltaTime;
			GetComponent<Image> ().fillAmount = (cooldownTime - currentTime) / cooldownTime;
		} else {
			GetComponent<Button>().interactable = true;
			cursor.SetCoolindDown (false);
		}
	}
		
	public void OnClick() {
		GameManager.Get ().SetActiveCursor (cursor, buttonDelegate);
	}
}
