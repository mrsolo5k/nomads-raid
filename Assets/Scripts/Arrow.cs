﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

	public GameObject target;
	public Vector3 targetMove;
	public float speed = 10.0f;
	public float damage = 15.0f;
	public float threshold = 32.0f;
	public float distance = 256.0f;
	public float estimatedDistance;

	private Vector3 velocity;

	void Start () {
		Object.Destroy (gameObject, 5.0f);
		if (target) {
			velocity = CalculateDirection ();
			transform.up = velocity.normalized;
		}
		estimatedDistance = distance;
	}

	Vector2 CalculateDirection() {

		AIController targetAI = target.GetComponent<AIController> (); 

		Vector3 totarget =  target.transform.position - transform.position;

		float a = Vector3.Dot(targetAI.velocity, targetAI.velocity) - (speed * speed);
		float b = 2 * Vector3.Dot(targetAI.velocity, totarget);
		float c = Vector3.Dot(totarget, totarget);

		float p = -b / (2 * a);
		float q = (float)Mathf.Sqrt((b * b) - 4 * a * c) / (2 * a);

		float t1 = p - q;
		float t2 = p + q;
		float t;

		if (t1 > t2 && t2 > 0) {
			t = t2;
		} else {
			t = t1;
		}

		Vector3 aimSpot = target.transform.position + targetAI.velocity * t;
		Vector3 bulletPath = aimSpot - transform.position;
	
		return bulletPath;
	}

	void FixedUpdate () {
		if (estimatedDistance > 0.0f && target) {
			transform.position += velocity.normalized * speed * GameManager.fixedTimeFactor();
			estimatedDistance -= speed;

			// TODO: consider changing to magnitude
			if ((Vector3.Distance (target.transform.position, transform.position) < threshold)) {
				transform.position = target.transform.position;
				transform.parent = target.transform;
				target.GetComponent<AIController> ().ReduceHealth (damage);
				target = null;
			}
		}
	}
}
